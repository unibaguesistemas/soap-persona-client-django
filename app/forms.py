"""
Definition of forms.
"""

from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.utils.translation import ugettext_lazy as _

class BootstrapAuthenticationForm(AuthenticationForm):
    """Authentication form which uses boostrap CSS."""
    username = forms.CharField(max_length=254,
                               widget=forms.TextInput({
                                   'class': 'form-control',
                                   'placeholder': 'User name'}))
    password = forms.CharField(label=_("Password"),
                               widget=forms.PasswordInput({
                                   'class': 'form-control',
                                   'placeholder':'Password'}))
class BuscarForm(forms.Form):
    idBuscar = forms.IntegerField(label='Buscar por Id')

class PersonaForm(forms.Form):
    id = forms.IntegerField(label='ID: ')
    #idPast = forms.IntegerField(widget=forms.HiddenInput())
    nombre = forms.CharField(max_length=100,label='Nombre: ')
    profesion = forms.CharField(max_length=100,label='Profesion: ')
    fecha = forms.DateField(label='Fecha de Nacimiento: ')
   
class EliminarForm(forms.Form):
    idEliminar = forms.IntegerField(label='Id para eliminar')