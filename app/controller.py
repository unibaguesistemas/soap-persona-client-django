
from zeep import Client
from .persona import Persona
class Controller:
    wsdl = 'http://192.168.1.4:7101/PersonaServidor-PersonaServidor-context-root/SWPersonaPort?wsdl'
    client = Client(wsdl)
   
    def buscar(self, i):
        p = self.client.service.buscarPersona(i)
        if p:
            return Persona(p['id'],p['nombre'],p['profesion'],p['fechaNacimiento'])
        else:
            return None
    def listar(self):  
        listaPersonas = []  
        lista = self.client.service.listarPersonas()
        for i in range(len(lista)):
            persona = Persona(lista[i]['id'],lista[i]['nombre'],lista[i]['profesion'],lista[i]['fechaNacimiento'])
            listaPersonas.append(persona)
        return listaPersonas
    def crear(self, persona):
        return self.client.service.agregarPersona(persona.comoArreglo())
    def eliminarPersona(self,i):
        return self.client.service.eliminarPersona(i)
    def actualizar(self, i, persona):
        return self.client.service.ModificarPersona(i,persona.comoArreglo())